package com.revature.q15;

public class Driver{

	public static void main(String[] args) {
		
		CalculatorImpl ci = new CalculatorImpl();
		int num1=10, num2=5;
		
		System.out.println(
			ci.addition(num1, num2)+"\n"+
			ci.subtraction(num1, num2)+"\n"+
			ci.multiplication(num1, num2)+"\n"+
			ci.division(num1, num2)
		);
		
	}

}
