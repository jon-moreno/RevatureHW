package com.revature.q15;

public interface CalculatorInterface {

	public int addition(int num1, int num2);
	public int subtraction(int num1, int num2);
	public int multiplication(int num1, int num2);
	public int division(int num1, int num2);
}
