package com.revature.q5;

import java.util.Scanner;

public class Substring {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String str = sc.next();
		int idx = sc.nextInt();
		System.out.println(substring(str, idx));
		
	}
	public static String substring(String str, int idx){
		char[] charArr=str.toCharArray();
		String substr="";
		
		for(int i=0; i<idx; i++){
			substr += charArr[i];
		}
		return substr;
	}
}