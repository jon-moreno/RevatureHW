package com.revature.q3;

import java.util.Scanner;

public class ReverseString {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		String str1, str2="";
		str1= sc.next();		
		
		for(int i=str1.length()-1; i >= 0; i--){
			//adds last character to end
			str2 += str1.charAt(i);
		}
		System.out.println(str2);
	}

}
