package com.revature.q9;
import java.util.ArrayList;
import com.revature.math.FindPrimes;

public class PrintPrimes extends FindPrimes{

	public static void main(String[] args) {
		//Creates ArrayList and fills it
		ArrayList arr = new ArrayList();
		for(int i=1; i<=100; i++){
			arr.add(i);
		}
		
		ArrayList primes=FindPrimes.returnPrimes(arr);
		System.out.println(primes);		
	}
}
