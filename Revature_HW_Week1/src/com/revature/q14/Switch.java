package com.revature.q14;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class Switch {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		switch(sc.nextInt())
		{
		case 1:
			//Find sqrt
			System.out.println("Find the sqrt of: ");
			System.out.println( Math.sqrt( sc.nextInt() ) );
			break;
		case 2:
			//Display today's date
			Date date = new Date();
			DateFormat dateFormat = new SimpleDateFormat("MMM d, yyyy");
			System.out.println(dateFormat.format(date));
			break;
		case 3:
			//Split String and store in array
			String string = "I am learning Core Java";
			String[] strArray = string.split(" ");
			System.out.println(Arrays.toString(strArray));
			break;
		default:
			break;
		}
		
	}

}
