package com.revature.q18;

import java.util.Scanner;

public class ConcreteClass extends AbstractClass{

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		String input = sc.nextLine();
		ConcreteClass cc = new ConcreteClass();
		
		System.out.print(
			cc.checkAnyUppers(input)+"\n"+
			cc.convertLowerToUpper(input)+"\n"
			);
		
		try{
			System.out.println(cc.addTen(input));
		}
		catch(NumberFormatException nfe){
			System.out.println("String wasn't number");
		}
	}

	@Override
	public boolean checkAnyUppers(String input) {
		
		for (char toCheck : input.toCharArray()) {
			if(Character.isUpperCase(toCheck)){
				return true;//Ends loop at first upper
			}
			else{
				continue;
			}
		}
		
		return false;//Only returned if no uppers
	}

	@Override
	public String convertLowerToUpper(String input) {
		return input.toUpperCase();
	}

	@Override
	public int addTen(String input) throws NumberFormatException{
/*		int strPlusTen=10;
		Integer.decode(input);
		Integer.valueOf(input);
		Integer.getInteger(input);
		return(strPlusTen);*/

		/*Integer.getInteger(input);
		Integer.decode(input);
		Integer.valueOf(input);*/
		//int blahblah = blah.intValue();
		return(Integer.parseInt(input)+10);
		//return(blahblah);
	}

}
