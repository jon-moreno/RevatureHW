package com.revature.q18;

public abstract class AbstractClass {

	public abstract boolean checkAnyUppers(String input);
	public abstract String convertLowerToUpper(String input);
	public abstract int addTen(String input);
}
