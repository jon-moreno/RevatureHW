package com.revature.q1;

import java.util.Arrays;

public class BubbleSort {

	public static void main(String[] args) {

		int[] arr = {1,0,5,6,3,2,3,7,9,8,4};
		int n = arr.length;
		
		//Step 2. Repeat for length of array.
		for(int i = 0; i < arr.length; i++){
			
			//Step 1. Compare neighbor ints, until largest value is last index
			for(int j = 1; j < (arr.length-i); j++){
				if(arr[j-1]>arr[j]){
					 int temp = arr[j-1];  
                     arr[j-1] = arr[j];  
                     arr[j] = temp;  
				}
				
			}
		}
		
		System.out.println(Arrays.toString(arr));
	}

}
