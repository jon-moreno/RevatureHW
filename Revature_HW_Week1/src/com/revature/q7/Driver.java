package com.revature.q7;

import java.util.ArrayList;
import java.util.Comparator; //Java 8 woo!

public class Driver{

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ArrayList<Employee> employees = new ArrayList<Employee>();
		Employee emp1 = new Employee("Zadaai","Accounting",22);
		Employee emp2 = new Employee("Aaron","Technical Recruiting",21);
		employees.add(emp1);employees.add(emp2);
		
		//Can also sort by multiple fields, in order
		/*Comparator<? super Employee> c = Comparator.comparing(Employee::getName)
										.thenComparing(Employee::getDepartment)
										.thenComparingInt(Employee::getAge);*/
		
		Comparator<? super Employee> byName = Comparator.comparing(Employee::getName);
		Comparator<? super Employee> byDepartment = Comparator.comparing(Employee::getDepartment);
		Comparator<? super Employee> byAge = Comparator.comparing(Employee::getAge);

		employees.sort(byName);
		System.out.println(employees.toString());
		employees.sort(byDepartment);
		System.out.println(employees.toString());
		employees.sort(byAge);
		System.out.println(employees.toString());

	}

}
