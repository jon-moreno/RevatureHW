package com.revature.q19;

import java.util.ArrayList;
import com.revature.math.FindPrimes;

public class RemovePrimes extends FindPrimes{
	static int sumOfEven = 0;
	static int sumOfOdd = 0;

	public static void main(String[] args) {
		//Creates ArrayList and fills it
		ArrayList<Integer> arr = new ArrayList<Integer>();
		for(int i=1; i<=10; i++){
			arr.add(i);
		}
		
		//Displays ArrayList
		System.out.println(arr);
		
		//Iterates ArrayList, and adds
		for(int i=0; i<arr.size(); i++){
			int value = (int)arr.get(i);
			
			//Evens
			if(value%2==0){
				sumOfEven += value;
			}
			//Odds
			else{
				sumOfOdd += value;
			}
		}
		
		System.out.println("Sum of Even: "+sumOfEven+"\n"+
							"Sum of Odd: "+sumOfOdd);
		
		//ID Primes
		ArrayList<Integer> primes=FindPrimes.returnPrimes(arr);

		//Remove Primes
		for(Object i : primes) {
			arr.remove(i);
		}
		System.out.println("Array w/o primes: "+arr);
		
		//ASK ABOUT ITERATING ARRAYLIST WHILE MODIFYING IT
		
	}

/*	//http://www.javawithus.com/programs/prime-numbers
	private static boolean isPrime(int n) {
		if (n <= 1) {
		       return false;
		   }
		   for (int i = 2; i < Math.sqrt(n); i++) {
		       if (n % i == 0) {
		           return false;
		       }
		   }
		   return true;
	}
*/
}
