package com.revature.q17;

import java.util.Scanner;

public class InterestCalculator {
//This class would reflect interest on something like a CD, where interest is applied only to the initial balance
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("What is the principal/starting balance?");
		float principal = sc.nextFloat();
		System.out.println("What is the percent interest rate?");
		float rate = sc.nextFloat()/100;
		System.out.println("How many (whole) years of interest?");
		int time = sc.nextInt();
		sc.close();

		System.out.println("You will accrue this much interest:"+principal*rate*time);
		System.out.println("Your balance will be: "+(principal+(principal*rate*time)));
	}

}
