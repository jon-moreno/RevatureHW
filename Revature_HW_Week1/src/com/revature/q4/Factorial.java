package com.revature.q4;

import java.util.Scanner;

public class Factorial {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		long result = 1;
		
		for(int i = 0; i<n; i++){
			result *= (n-i);
		}
		
		System.out.println(result);
	}

}
