package com.revature.q8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Palindromes {

	public static void main(String[] args) {
		List<String> l = Arrays.asList(	"karan",
										"madam",
										"tom",
										"civic",
										"radar",
										"sexes",
										"jimmy",
										"kayak",
										"john",
										"refer",
										"billy",
										"did"
									);
		//Stream.of("xyz", "abc").collect(Collectors.toList()); //Java 8 possible sln
		
		ArrayList<String> strings = new ArrayList<String>(l);
		ArrayList<String> palindromes = new ArrayList<String>();
		System.out.println(strings.toString());
				
		for (String string : strings) {
			
			//Implementation using builtin reverse
			if(string.equals(new StringBuilder(string).reverse().toString())) {
				palindromes.add(string);
			}
			
			//Implementation storing reversed string
/*			char[] chars = string.toCharArray();
			String revString = "";
			
			for (int i=chars.length-1; i>-1; i--) {
				revString += chars[i];
			}
			
			if(string.equals(revString)) {
				palindromes.add(string);
			}*/
			
			//Implementation comparing opposite indices
			/*if(isPal(string)) {
				palindromes.add(string);
			}*/
		}
		
		System.out.println(palindromes.toString());
	}
	
	public static boolean isPal(String string){
		int i = 0;
		int j = string.length()-1;
		
		for (int k = 0; k < string.length()/2; k++,i++,j--) {				
			if(string.charAt(i) != (string.charAt(j))){
					return false;
			}
			else 
			{
				return true;
			}
		}
		return false;
	}
}
