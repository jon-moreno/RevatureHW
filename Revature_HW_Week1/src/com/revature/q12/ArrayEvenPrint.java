package com.revature.q12;

import java.util.stream.IntStream;

public class ArrayEvenPrint {
	public static void main(String[] args) {
	
		final int arrSize = 100;
		int[] arr = new int[arrSize];
			
		//Store
		for (int i = 0; i < arr.length; i++) {
			arr[i] = i+1;
		}
		
		//Print with enhanced For Loop
		for (int i : arr) {
			System.out.print((i%2 == 0)?i+"\n":"");
		}
}
}
