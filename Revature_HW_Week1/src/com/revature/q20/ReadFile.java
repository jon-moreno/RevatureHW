package com.revature.q20;

import java.io.*;
import java.util.Scanner;

public class ReadFile {
	
	static float sum = 0;
	static float count = 0;

	public static void main(String[] args) throws IOException{
		//Ask for filename		
		try{
			//File Object
			File file = new File("Data.txt");//BufferedReader br = new BufferedReader(new FileReader(file));
			//FileReader Object
			FileReader fr = new FileReader(file);
			//BufferedReader Object
			BufferedReader br = new BufferedReader(fr);
			
			//Scanner Implementation
			/*Scanner fileIn = new Scanner(file);		
			while(fileIn.hasNextLine()){
				String row = fileIn.nextLine();
				String[] rowData = row.split(":");
				System.out.println(
						"Name: "+rowData[0]+" "+rowData[1]+"\n"+
						"Age: "+rowData[2]+" years"+"\n"+
						"State: "+rowData[3]+" State"+"\n"
						);
			}*/
			
			//File IO implementation
			String currentRow;
			while((currentRow = br.readLine()) != null){
				String[] rowData = currentRow.split(":");
				System.out.println(
						"Name: "+rowData[0]+" "+rowData[1]+"\n"+
						"Age: "+rowData[2]+" years"+"\n"+
						"State: "+rowData[3]+" State"+"\n"
						);
			}
			
			//fileIn.close();
			br.close();
			fr.close();
			
		}
		catch(FileNotFoundException fnf){
			fnf.printStackTrace();
		}
		catch(Exception e){
			e.printStackTrace();
		}

		//Only runs if file exists
/*		userIn.close();
		Scanner fileIn = new Scanner(file);
		while(fileIn.hasNextLine()){

			count += 1;
			String row = fileIn.nextLine();
			String[] rowData = row.split(":");
			System.out.println(
					"Name: "+rowData[0]+" "+rowData[1]+"\n"+
					"Age: "+rowData[2]+" years"+"\n"+
					"State: "+rowData[3]+" State"+"\n"
					);
			sum += Float.parseFloat(rowData[2]);
						*/
		}
		//fileIn.close();
		//System.out.print("Average: "+sum/count);
		
		}


