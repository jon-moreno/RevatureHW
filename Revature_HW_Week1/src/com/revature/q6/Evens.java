package com.revature.q6;

import java.util.Scanner;

public class Evens {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();
		System.out.println(isEven(num));
	}

	private static boolean isEven(int num) {
		//Determines evenness by whether rounded in integer division
		if((num/2)*2 != num){
			return false;
		}
		else{
			return true;
		}
	}

}
