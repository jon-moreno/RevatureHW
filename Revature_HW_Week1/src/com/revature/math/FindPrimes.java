package com.revature.math;

import java.util.ArrayList;

public class FindPrimes {

	public static ArrayList<Integer> returnPrimes(ArrayList<Integer> al) {
		ArrayList<Integer> primes = new ArrayList<Integer>();
		
		for(int i : al) {
			if(isPrime(i)) {
				primes.add(i);
			}
		}
		return primes;
	}

	public static boolean isPrime(int i) {
		
		//Always Non-prime
		if(i<=1) {
			return false;
		}
		
		//Can't be used in test
		if(i==2){
			return true;
		}
		
		//Assume prime by default
		boolean result = true;
		
		for(int j=2; j<(1+(i/2)); j++) {
			if(i%j == 0) {
				result = false;
				break;//immediately exit if not prime
			}
			
			else {
				result = true;
			}
			
		}
		return result;
	}
}
