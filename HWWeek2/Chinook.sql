--# 2) SQL Queries

--### 2.1 SELECT
Select * from employee;

Select * from employee
where lastname='King';

Select * from "EMPLOYEE"
Where "FIRSTNAME"='Andrew'
And REPORTSTO IS null;

--### 2.2 ORDER BY
Select title
from album
Order By title Desc;

Select firstname
from customer
order by city;

--### 2.3 INSERT INTO
insert into genre(genreid, name)
values(26, 'Soundclown');
insert into genre values(27, 'Mashup');
select * from genre;

select * from employee;
insert into employee(employeeid, lastname, firstname) VALUES(9, 'Doe', 'Jane');
insert into employee(employeeid, lastname, firstname)
values(10, 'Smith', 'Alice');

insert into CUSTOMER(customerid, firstname, lastname, email)values(60, 'Jane', 'Doe','tempmail');
insert into customer(customerid, firstname, lastname, email)values(61, 'Alice', 'Smith','tempmail');
select * from customer order by customerid desc;

--### 2.4 UPDATE
select * from customer
where customerid=32;
update customer
set lastname='Walter', firstname='Robert'
where lastname='Mitchell' and firstname='Aaron';

update artist
set NAME='CCR'
where name='Creedence Clearwater Revival';
select * from artist where name='CCR';

select FirstName, City
from customer
Order by city desc;

--### 2.5 LIKE
select *
from invoice
where BILLINGADDRESS
like 'T%';

--### 2.6 BETWEEN
Select *
from INVOICE
Where TOTAL
Between 15 and 50;

select * from employee
where hiredate
between '01-Jun-03'
And '01-Mar-04';

--### 2.7 DELETE
ALTER TABLE INVOICE
ADD CONSTRAINT FK_INVOICECUSTOMERID FOREIGN KEY
(
  CUSTOMERID 
)
REFERENCES CUSTOMER
(
  CUSTOMERID 
)
ON DELETE CASCADE ENABLE;

ALTER TABLE INVOICELINE 
DROP CONSTRAINT FK_INVOICELINEINVOICEID;

ALTER TABLE INVOICELINE
ADD CONSTRAINT FK_INVOICELINEINVOICEID FOREIGN KEY
(
  INVOICEID 
)
REFERENCES INVOICE
(
  INVOICEID 
)
ON DELETE CASCADE ENABLE;

delete from customer
where firstname='Robert'
and lastname='Walter';

--# 3) SQL Functions

--### 3.1 System Defined Functions
select CURRENT_TIMESTAMP
from EMPLOYEE
Where ROWNUM=1;

SELECT * FROM mediatype where rownum=1;

--### 3.2 System Defined Aggregate Functions
Select AVG(total)
From invoice;

Select MAX(unitprice)
from Track;

--### 3.3 User Defined Scalar Functions
CREATE OR REPLACE FUNCTION AVGINVLINEFUNC 
(
  Average Out Number 
) RETURN Number AS 
BEGIN
  Select Avg(unitprice) into average FROM invoiceline;
  RETURN Average;
END AVGINVLINEFUNC;

--### 3.4 User Defined Table Valued Functions

--# 4) Stored Procedures

--### 4.1 Basic Stored Procedure
create or replace PROCEDURE BASICSTOREDPROC 
(
  EMPROW OUT SYS_REFCURSOR
) AS 
BEGIN
  OPEN EMPROW FOR
  SELECT Firstname, LASTNAME
  FROM CUSTOMER;
END BASICSTOREDPROC;

--### 4.2 Stored Procedure Input Parameters
CREATE OR REPLACE PROCEDURE INPUTPROC1 
(
  NEWLNAME IN VARCHAR2 
, FNAME IN VARCHAR2
) AS 
BEGIN
  UPDATE EMPLOYEE
  SET LASTNAME=NEWLNAME
  WHERE FIRSTNAME=FNAME;
END INPUTPROC1;
--------------------------------
CREATE OR REPLACE PROCEDURE SELECTMGRPROC 
(
  EMPID IN NUMBER 
, EMPNAME OUT VARCHAR2 
, MGRNAME OUT VARCHAR2 
) AS 
BEGIN
Select emp.Firstname, mgr.Firstname into EMPNAME, MGRNAME
From Employee emp, Employee mgr
Where emp.Reportsto=mgr.employeeid
AND emp.employeeid=EMPID;
END SELECTMGRPROC;
----------------------------------

--### 4.3 Stored Procedure Output Parameters
CREATE OR REPLACE PROCEDURE OUTPUTPROC 
(
  CUSTID IN NUMBER 
, CUSTNAMEOUT OUT VARCHAR2 
, CUSTCOMPANY OUT VARCHAR2 
) AS 
BEGIN
  SELECT FIRSTNAME, COMPANY into CUSTNAMEOUT, CUSTCOMPANY
  From CUSTOMER
  WHERE CUSTOMERID=CUSTID;
END OUTPUTPROC;

--# 5) Transactions
SAVEPOINT;
SET TRANSACTION;
DELETE FROM INVOICE
WHERE INVOICEID=216;
ROLLBACK;
COMMIT; 

CREATE OR REPLACE PROCEDURE TRANSACTINSERTPROC 
(
  PARAM1 IN NUMBER 
, PARAM2 IN VARCHAR2 
, PARAM3 IN VARCHAR2 
, PARAM4 IN VARCHAR2
) AS 
BEGIN
SET TRANSACTION NAME 'Insert Customer';
INSERT INTO customer(CUSTOMERID, FIRSTNAME, LASTNAME, EMAIL) 
VALUES (PARAM1, PARAM2, PARAM3, PARAM4);
COMMIT; 
END TRANSACTINSERTPROC;

--# 6) Triggers

--### 6.1 AFTER/FOR
CREATE OR REPLACE TRIGGER AFTERINSTRIG 
AFTER INSERT ON EMPLOYEE 
BEGIN
DBMS_OUTPUT.PUT_LINE('Inserted');
END;

create or replace TRIGGER AFTERUPDATETRIG 
AFTER UPDATE OF LASTNAME ON EMPLOYEE 
BEGIN
DBMS_OUTPUT.PUT_LINE('Looks like someone got hitched or divorced.');
END;

create or replace TRIGGER AFTERDELTRIG 
AFTER DELETE ON EMPLOYEE 
BEGIN
DBMS_OUTPUT.PUT_LINE('Deleted');
END;

--# 7) Joins

--### 7.1 INNER
Select c.firstname, c.lastname, i.invoiceid
from customer c
INNER JOIN invoice i
On c.customerid = i.customerid;
--### 7.2 OUTER
Select c.customerid, c.firstname, c.lastname, i.invoiceid, i.total
from customer c
left join invoice i
on c.customerid=i.customerid;

--### 7.3 RIGHT
Select ar.name, al.title
From album al
right join artist ar
On ar.artistid=al.artistid;

--### 7.4 CROSS
Select a.title, t.name
from album a
cross join
track t
Order By a.artistid;
--### 7.5 SELF
Select emp.Firstname AS Employee, mgr.Firstname AS Manager
From Employee emp
Inner Join Employee mgr
On emp.reportsto = mgr.employeeid;

--Classwork
Select inv.InvoiceDate, inv.BillingState, il.unitprice, il.quantity
from invoice inv inner join invoiceline il
on il.Invoiceid=inv.invoiceid;

Select inv.InvoiceDate, inv.BillingState, il.unitprice, il.quantity
from invoice inv right join invoiceline il
on il.Invoiceid=inv.invoiceid;

select *
from track t left join album a
on t.albumid = a.albumid;